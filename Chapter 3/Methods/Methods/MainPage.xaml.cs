﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices.WindowsRuntime;
using Windows.Foundation;
using Windows.Foundation.Collections;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Controls.Primitives;
using Windows.UI.Xaml.Data;
using Windows.UI.Xaml.Input;
using Windows.UI.Xaml.Media;
using Windows.UI.Xaml.Navigation;

// The Blank Page item template is documented at http://go.microsoft.com/fwlink/?LinkId=402352&clcid=0x409

namespace Methods
{
    /// <summary>
    /// An empty page that can be used on its own or navigated to within a Frame.
    /// </summary>
    public sealed partial class MainPage : Page
    {
        public MainPage()
        {
            this.InitializeComponent();
        }

        private void OnCalculateClick(object sender, RoutedEventArgs e)
        {
            int calculatedValue = 0;

            try
            {
                int leftHandSide = System.Int32.Parse(_txtLhsOperand.Text);
                int rightHandSide = System.Int32.Parse(_txtRhsOperand.Text);

                if (_rbtnAddition.IsChecked.HasValue && _rbtnAddition.IsChecked.Value)
                {
                    calculatedValue = AddValues(leftHandSide, rightHandSide);
                    ShowResult(calculatedValue);
                }
                else if (_rbtnSubtraction.IsChecked.HasValue && _rbtnSubtraction.IsChecked.Value)
                {
                    calculatedValue = SubtractValues(leftHandSide, rightHandSide);
                    ShowResult(calculatedValue);
                }
                else if (_rbtnMultiplication.IsChecked.HasValue && _rbtnMultiplication.IsChecked.Value)
                {
                    calculatedValue = MultiplyValues(leftHandSide, rightHandSide);
                    ShowResult(calculatedValue);
                }
                else if (_rbtnIntDivision.IsChecked.HasValue && _rbtnIntDivision.IsChecked.Value)
                {
                    calculatedValue = IntDivideValues(leftHandSide, rightHandSide);
                    ShowResult(calculatedValue);
                }
                else if (_rbtnRemainder.IsChecked.HasValue && _rbtnRemainder.IsChecked.Value)
                {
                    calculatedValue = RemainderValues(leftHandSide, rightHandSide);
                    ShowResult(calculatedValue);
                }
            }
            catch (Exception caught)
            {
                _txtExpression.Text = "";
                _txResult.Text = caught.Message;
            }
        }

        private int AddValues(int leftHandSide, int rightHandSide)
        {
            _txtExpression.Text = $"{leftHandSide} + {rightHandSide}";
            return leftHandSide + rightHandSide;
        }

        private int SubtractValues(int leftHandSide, int rightHandSide)
        {
            _txtExpression.Text = $"{leftHandSide} - {rightHandSide}";
            return leftHandSide - rightHandSide;
        }

        private int MultiplyValues(int leftHandSide, int rightHandSide)
        {
            _txtExpression.Text = $"{leftHandSide} * {rightHandSide}";
            return leftHandSide * rightHandSide;
        }

        private int IntDivideValues(int leftHandSide, int rightHandSide)
        {
            _txtExpression.Text = $"{leftHandSide} / {rightHandSide}";
            return leftHandSide / rightHandSide;
        }

        private double ReadDivideValues()
        {
            //TODO: Make the real number division work in the program
            return 0;
        }

        private int RemainderValues(int leftHandSide, int rightHandSide)
        {
            _txtExpression.Text = $"{leftHandSide} % {rightHandSide}";
            return leftHandSide % rightHandSide;
        }

        private void ShowResult(int answer) => _txResult.Text = answer.ToString();
    }
}
